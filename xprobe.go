package main

import (
	"log"

	"github.com/BurntSushi/xgbutil"
)

func main() {
	_, err := xgbutil.NewConn()
	if err != nil {
		log.Fatal(err)
	}
}
