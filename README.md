Minimal utility to check if we are able to talk to the X server.

Build with `go build`

Usage example: `./xprobe && echo "All good!" || echo "Oh noes!"`
